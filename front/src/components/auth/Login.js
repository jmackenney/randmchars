import React, { useState, useEffect } from "react";

const Login = () => {
  const [user, setUser] = useState({
    username: "",
    password: "",
  });

  const { username, password } = user;

  const onChange = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (username.trim() === "" || password === "") {
      alert("Please, fill username and password");
    } else {
      fetch("http://localhost:8081/api/users", {
        headers: { "Content-Type": "application/json" },
        method: "post",
        body: JSON.stringify({ user: username, pass: password }),
      })
        .then((res) => {
          if (res.status == 401) {
            alert("Username or password invalid!");
          } else if (res.status == 200) {
            localStorage.setItem("userLogged", true);
            localStorage.setItem("userName", username);
            window.location = "/private";
          } else {
            alert("Something wrong! Please, try again.");
          }
        })
        .catch((err) => {
          alert("Something wrong! Please, try again later.");
        });
    }
  };
  useEffect(() => {
    if (localStorage.getItem("userLogged")) {
      window.location = "/private";
    }
  });
  return (
    <div className="form-user">
      <div className="container-form shadow-dark">
        <h1>Login</h1>
        <form onSubmit={onSubmit}>
          <div className="field-form">
            <label htmlFor="username">User</label>
            <input
              id="username"
              name="username"
              placeholder="Username"
              value={username}
              onChange={onChange}
            />
          </div>
          <div className="field-form">
            <label htmlFor="password">Password</label>
            <input
              id="password"
              name="password"
              type="password"
              placeholder="************"
              value={password}
              onChange={onChange}
            />
          </div>
          <div className="field-form">
            <input
              type="submit"
              className="btn btn-prim btn-block"
              value="Log in"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
