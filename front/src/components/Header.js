import React from "react";

const Header = () => {
  const logout = () => {
    fetch(`http://localhost:8081/api/logout`).then((res) => {
      if (res.status === 200) {
        localStorage.clear();
        window.location = "/";
      }
    });
  };
  return (
    <header className="app-header">
      <p className="username">
        Hello! <span>{localStorage.getItem("userName")}</span>
      </p>
      <nav className="nav">
        <a href="#!" onClick={logout}>
          Logout
        </a>
      </nav>
    </header>
  );
};

export default Header;
