import React, { Fragment, useEffect, useState } from "react";
import Header from "./Header";
import { Waypoint } from "react-waypoint";

const Private = () => {
  const [charaList, setcharaList] = useState([]);
  const [page, setPage] = useState(1);

  const getNextPage = () => {
    setPage(page + 1);
    fetch(`http://localhost:8081/api/randm?page=${page}`)
      .then((res) => {
        res.json().then((r) => {
          let merged = [].concat(charaList, r);
          setcharaList(merged);
        });
      })
      .catch((e) => console.log(e));
  };
  useEffect(() => {
    fetch(`http://localhost:8081/api/randm?page=${page}`)
      .then((res) => {
        if (res.status === 401) {
          window.location = "/";
        } else if (res.status === 200) {
          res.json().then((r) => {
            setcharaList(r);
          });
        } else {
          alert("Something wrong happen!");
        }
      })
      .catch((e) => console.log(e));
  }, []);
  useEffect(() => {
    if (!localStorage.getItem("userLogged")) {
      window.location = "/";
    }
  });
  return (
    <Fragment>
      <div className="app-container">
        <Header />
        <br />
        <h2>Characters</h2>
        <div className="chara-list">
          {charaList.length === 0 ? (
            <li>No characters returning...</li>
          ) : (
            charaList.map((char) => (
              <div className="chara shadow" key={char.id}>
                <div>
                  <img className="chara-image" src={char.image} />
                </div>
                <div className="chara-container">
                  <p>Name: {char.name}</p>
                  <p>Status: {char.status}</p>
                  <p>Species: {char.species}</p>
                  <p>Gender: {char.gender}</p>
                </div>
              </div>
            ))
          )}
        </div>
        {charaList.length > 0 ? "Loading more items..." : ""}

        <Waypoint onEnter={getNextPage} />
        <br />
      </div>
    </Fragment>
  );
};

export default Private;
