var express = require("express");
var request = require("request");
var app = express(),
  session = require("express-session"),
  cookieParser = require("cookie-parser"),
  bodyParser = require("body-parser");
app.use(cookieParser());
app.use(bodyParser.json());
app.use(
  session({
    secret: "top-secret",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false },
    maxAge: 120 * 1000,
  })
);
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
var sess;
app.post("/api/create-user", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  request.post(
    {
      url: "http://redis-app:3000/api/create-user",
      form: { username: username, password: password },
    },
    (err, r) => {
      if (r.statusCode == 200) {
        res.status(200).send({
          status: "created",
        });
      } else {
        res.status(400).send({
          status: "error",
        });
      }
    }
  );
});

app.post("/api/users", (req, res) => {
  const user = req.body.user;
  const pass = req.body.pass;
  request(`http://redis-app:3000/api/user/${user}`, (err, body) => {
    const obj = JSON.parse(body.body);
    if (obj.user && obj.user.username == user && obj.user.password == pass) {
      req.session.username = user;
      req.session.save();
      sess = req.session.username;
      res.status(200).send({
        isValid: true,
        username: req.session.username,
      });
    } else {
      res.status(401).send({ isValid: false });
    }
  });
});

app.get("/api/logout", (req, res) => {
  sess = null;
  req.session.destroy();
  res.status(200).send({ logged: false });
});

app.get("/", (req, res) => {
  res.send("Working!");
});

app.get("/api/randm", (req, res) => {
  if (sess)
    request(
      `http://rickandmortyapi.com/api/character?page=${req.query.page}`,
      (err, body) => {
        const obj = JSON.parse(body.body);
        const result = [];
        if (obj.results) {
          obj.results.map((m) => {
            result.push({
              id: m.id,
              name: m.name,
              status: m.status,
              species: m.species,
              gender: m.gender,
              image: m.image,
            });
          });
          res.status(200).send(result);
        }
      }
    );
  else res.status(401).send({ unauthorized: true });
});

app.listen(8081, () => {});
