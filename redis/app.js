let express = require("express");
let bodyParser = require("body-parser");

let port = 3000;
let app = express();

app.use(express.static(__dirname + "/public"));
app.set("view engine", "ejs");

let redis = require("redis");
let client = redis.createClient({
  host: "redis-server",
  port: 6379,
});
client.on("connect", () => {
  console.log("Redis connected successfully!!!!!");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.render("users");
});

app.post("/users/search", (req, res) => {
  let username = req.body.username;
  client.hgetall(username, (err, data) => {
    if (!data) {
      res.render("users", { message: "No user exist" });
    } else {
      data.username = username;
      res.render("users", { users: data });
    }
  });
});

app.get("/users/add", (req, res) => {
  res.render("add-user");
});
app.get("/api/user/:user", (req, res) => {
  const user = req.params.user;
  client.hgetall(user, (err, data) => {
    res.status(200).send({ user: data });
  });
});

app.post("/api/create-user", (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  client.hmset(
    username,
    ["username", username, "password", password],
    (err, response) => {
      if (err) {
        res.status(400).send({ error: err });
      } else {
        console.log(response);
        res.status(200).send({ status: "created" });
      }
    }
  );
});

app.post("/users/add", (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  client.hmset(
    username,
    ["username", username, "password", password],
    (err, response) => {
      if (err) {
        console.log(err);
      } else {
        console.log(response);
        res.render("add-user", { message: "User added successfully!!!!" });
      }
    }
  );
});

app.get("/users/delete/:username", (req, res) => {
  client.del(req.params.username, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect("/");
    }
  });
});

app.listen(port, () => {
  console.log("Server running at port " + port);
});
